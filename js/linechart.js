var lineChartData = {
    labels: ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"],
    datasets: [{
        fillColor: "rgba(37, 201, 210, 0.5)",
        strokeColor: "#25c9d2",
        pointColor: "#25c9d2",
        data: [20, 30, 40, 50, 60, 60, 40]
    }, {
        fillColor: "rgba(244, 189, 129, 0.5)",
        strokeColor: "#f4bd81",
        pointColor: "#f4bd81",
        data: [10, 30, 25, 30, 50, 65, 70]
    }]

}

Chart.defaults.global.animationSteps = 50;
Chart.defaults.global.tooltipYPadding = 4;
Chart.defaults.global.tooltipCornerRadius = 0;
Chart.defaults.global.tooltipTitleFontStyle = "normal";
Chart.defaults.global.tooltipFillColor = "rgba(0,160,0,0.8)";
Chart.defaults.global.animationEasing = "linear";
Chart.defaults.global.responsive = true;
Chart.defaults.global.scaleLineColor = "rgba(0,0, 0, 0.05)";
Chart.defaults.global.scaleFontSize = 14;

var ctx = document.getElementById("lineChart").getContext("2d");
var LineChartDemo = new Chart(ctx).Line(lineChartData, {
    pointDotRadius: 2,
    bezierCurve: true,
    scaleShowVerticalLines: false,
    scaleGridLineColor: "rgba(0,0, 0, 0.05)"
});