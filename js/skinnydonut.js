/*!
 * Chart.js
 * http://chartjs.org
 *
 * Copyright 2013 Nick Downie
 * Released under the MIT license
 * https://github.com/nnnick/Chart.js/blob/master/LICENSE.md
 */

// ======================================================
// Doughnut Chart
// ======================================================

// Doughnut Chart Options
var skinnydoughnutOptions = {
  //Boolean - Whether we should show a stroke on each segment
  segmentShowStroke : false,
  
  //String - The colour of each segment stroke
  segmentStrokeColor : "#fff",
  
  //Number - The width of each segment stroke
  segmentStrokeWidth : 0,
  
  //The percentage of the chart that we cut out of the middle.
  percentageInnerCutout : 90,
  
  //Boolean - Whether we should animate the chart 
  animation : true,
  
  //Number - Amount of animation steps
  animationSteps : 100,
  
  //String - Animation easing effect
  animationEasing : "easeInOutCubic",
  
  //Boolean - Whether we animate the rotation of the Doughnut
  animateRotate : true,

  //Boolean - Whether we animate scaling the Doughnut from the centre
  animateScale : false,
  
  //Function - Will fire on animation completion.
  onAnimationComplete : null,

  responsive: true
}


// Doughnut Chart Data
var skinnydoughnutData = [
{
  value: 77,
  color:"white"
},
{
  value : 23,
  color : "rgba(255,255,255,0.05)"
}

]


//Get the context of the Doughnut Chart canvas element we want to select
var ctx = document.getElementById("skinnydoughnutChart").getContext("2d");

// Create the Doughnut Chart
var myskinnydoughnutChart = new Chart(ctx).Doughnut(skinnydoughnutData, skinnydoughnutOptions);